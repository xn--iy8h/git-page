---
---
# Testing GitLab Pages ...

Nothing more than a simple [page](https://gitlab.com/xn--iy8h/git-page/) to test git-page deployment on [https://*.gitlag.io](https://pages.gitlab.io/)
if you'd like to do the same, you can fork this repository with the command:

```sh
git clone https://gitlab.com/xn--iy8h/git-page.git my-git-page
```

Git-Pages: see also:
* [gitlab-pages](https://gitlab.com/pages/plain-html/)
* [getting started](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)

--&nbsp;
<br>this page: <https://xn--iy8h.gitlab.io/git-page>
