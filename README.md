## GitLab Pages ...


This page is a CI/CD-deployed page from gitlab.

It is build using Jekyll (don't forget the front matter in your markdown file : '---\n---\n').

It is available at <https://🕸.gitlab.io/git-page/>


